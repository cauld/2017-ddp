#include "defi.h"

#include <string.h>
#include <stdbool.h>

int main(void)
{
    Dictionary *data = loadTextFile("FR_len05.dico");
    if(!data)
		return -1;


    Graph *gr = rabbitJump(data);
    if(!gr)
    {
        freeDictionary(data);
        return -1;
    }

    // détermine le mot qui produit le plus de bonds
    size_t maximum = 0;
    wordID wordNumberMax = 0;

    for(size_t i = 0; i < gr->headSize; i++)
    {
        if(gr->head[i]->adjNodeArraySize > maximum)
        {
            maximum = gr->head[i]->adjNodeArraySize;
            wordNumberMax = (wordID)i;
        }
    }

    printf("Le mot qui produit le plus de bonds :\n");
    display(gr, getWord(gr->dico_ref, wordNumberMax));
    printf("%zu bonds\n\n", maximum);
    printf("EXIT pour quitter\n");

    bool quit = false;

    while(!quit)
    {
        char word[WORD_SIZE + 1];

        printf("mot : ");
        scanf("%5s", word);

        int c;
        while((c = getchar()) != EOF && c != '\n');

        if(!strcmp(word, "EXIT"))
            quit = true;

        else if(strlen(word) != WORD_SIZE)
            fprintf(stderr, "le mot doit avoir %d lettres\n\n", WORD_SIZE);

        else
            display(gr, word);
    }


    freeGraph(gr);
    freeDictionary(data);

    return 0;
}
