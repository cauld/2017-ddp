#include <stdio.h>
#include <stdlib.h>

#define WORD_SIZE 5
#define ARRAY_SIZE 81

typedef struct
{
    char *data;
    size_t size;
} TextData;

typedef struct
{
    int min;
    int max;
} MinMaxValue;


TextData *loadTextFile(const char* filename);
void freeTextData(TextData *td);
char *getWord(const TextData *td, size_t num);
void drawPattern(const char *word);
int *convertWordsToPatterns(const TextData *td);
void drawPatternFromInt(int num);
MinMaxValue getMinMax(int *array, size_t size);

TextData *loadTextFile(const char* filename)
{
    TextData *td = malloc(sizeof(TextData));

    if(!td)
    {
        perror("malloc");
        return NULL;
    }


    FILE *fp = fopen(filename, "r");

    if(!fp)
    {
        perror("fopen");
        free(td);
        return NULL;
    }

    fseek(fp,0, SEEK_END);
    size_t length = (size_t)ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char *buffer = malloc(length * sizeof(char)); // pas de length+1 car après le dernier mot, il y a un retour à la ligne

    if(!buffer)
    {
        perror("malloc");
        fclose(fp);
        free(td);
        return NULL;
    }


    int c;
    size_t i = 0;

    while((c = getc(fp)) != EOF)
    {
        if(c != '\n')
            buffer[i] = (char)c;

        else
            buffer[i] = '\0';

        i++;
    }


    fclose(fp);

    td->data = buffer;
    td->size = length / (WORD_SIZE + 1);

    return td;
}

void freeTextData(TextData *td)
{
    free(td->data);
    free(td);
}

char *getWord(const TextData *td, size_t num)
{
    if(td->size > num)
        return td->data + (WORD_SIZE + 1) * num;


    fprintf(stderr, "index out of bounds\n");

    return NULL;
}

void drawPattern(const char *word)
{
    size_t i = 1;

    while(word[i] != '\0')
    {
        if(word[i - 1] < word[i])
            printf("<");

        else if(word[i - 1] == word[i])
            printf("=");

        else
            printf(">");

        i++;
    }

    printf("\n");
}

int *convertWordsToPatterns(const TextData *td)
{
    /* codage des symboles
       0 : <
       1 : =
       2 : >
    */

    int *array = calloc(td->size, sizeof(int));

    if(!array)
    {
        perror("calloc");
        return NULL;
    }

    for(size_t i = 0; i < td->size; i++)
    {
        size_t j = i * (WORD_SIZE + 1) + 1;

        while(td->data[j] != '\0')
        {
            array[i] *= 4;

            if(td->data[j - 1] < td->data[j])
                array[i] += 0;

            else if(td->data[j - 1] == td->data[j])
                array[i] += 1;

            else
                array[i] += 2;

            j++;
        }
    }

    return array;
}

void drawPatternFromInt(int num)
{
    for(int i = 3; i >= 0; i--)
    {
        int x = (num >> (2 * i)) & 3;

        if(x == 0)
            printf("<");

        else if(x == 1)
            printf("=");

        else if(x == 2)
            printf(">");
    }

}

MinMaxValue getMinMax(int *array, size_t size)
{
    int max = array[0];
    int min = array[0];

    for(size_t i = 1; i < size; i++)
    {
        if(max < array[i])
            max = array[i];

        if(min > array[i])
            min = array[i];
    }

    MinMaxValue t = {min, max};

    return t;
}


int main(void)
{
    // nombre de mots dans FR_len05.dico : 7823
    TextData *tData = loadTextFile("FR_len05.dico");

    if(!tData)
        return -1;

    //printf("%s\n", getWord(tData, 7822));
    //drawPattern(getWord(tData, 7822));

    int *patterns = convertWordsToPatterns(tData);

    if(!patterns)
    {
        freeTextData(tData);
        return -1;
    }

    /* codage des symboles dans un nombre au format int
       0 : <
       1 : =
       2 : >

       exemple :
       <<<<   ->  0 * 4^3 + 0 * 4^2 + 0 * 4 + 0 = 0
       >==<   ->  2 * 4^3 + 1 * 4^2 + 1 * 4 + 0 = 148
     */
    int array[ARRAY_SIZE] = {0, 1, 2, 4, 5, 6, 8, 9, 10, 16, 17, 18, 20, 21, 22, 24, 25, 26,
                             32, 33, 34, 36, 37, 38, 40, 41, 42, 64, 65, 66, 68, 69, 70, 72,
                             73, 74, 80, 81, 82, 84, 85, 86, 88, 89, 90, 96, 97, 98, 100, 101,
                             102, 104, 105, 106, 128, 129, 130, 132, 133, 134, 136, 137, 138,
                             144, 145, 146, 148, 149, 150, 152, 153, 154, 160, 161, 162, 164,
                             165, 166, 168, 169, 170};


    int occurence[ARRAY_SIZE] = {0};

    // recherche pour chaque motif possible le nombre de mots ayant ce motif
    for(size_t i = 0; i < tData->size; i++)
        for(size_t j = 0; j < ARRAY_SIZE; j++)
        {
            if(array[j] == patterns[i])
                occurence[j]++;
        }

    // affichage
    printf("motif : nombre\n------|-------\n");

    for(size_t i = 0; i < ARRAY_SIZE; i++)
    {
        drawPatternFromInt(array[i]);
        printf("  : ");
        printf("%d\n", occurence[i]);
    }


    // le ou les motifs les plus fréquents
    MinMaxValue minMax = getMinMax(occurence, ARRAY_SIZE);

    printf("\nle ou les motifs les plus fréquents\n");

    for(size_t i = 0; i < ARRAY_SIZE; i++)
        if(occurence[i] == minMax.max)
        {
            drawPatternFromInt(array[i]);
            printf("\n");
        }



    // le ou les motifs les moins fréquents mais ayant au moins une occurrence
    printf("\nle ou les motifs les moins fréquents mais ayant au moins une occurrence\n");

    if(minMax.min == 0)
    {
        for(int i = minMax.min + 1; i <= minMax.max; i++)
        {
            for(size_t j = 0; j < ARRAY_SIZE; j++)
            {
                if(occurence[j] == i)
                {
                    minMax.min = i;
                    goto stop;
                }
            }
        }
    }

stop:

    for(size_t i = 0; i < ARRAY_SIZE; i++)
        if(occurence[i] == minMax.min)
        {
            drawPatternFromInt(array[i]);
            printf("\n");
        }



    // motifs manquants
    printf("\nmotifs manquants\n");

    for(size_t i = 0; i < ARRAY_SIZE; i++)
        if(occurence[i] == 0)
        {
            drawPatternFromInt(array[i]);
            printf("\n");
        }


    free(patterns);
    freeTextData(tData);

    return 0;
}
/*
for(int i = 0; i <= 2; i++)
    for(int j = 0; j <= 2; j++)
        for(int k = 0; k <=2; k++)
            for(int l = 0; l <= 2; l++)
                printf("%d\n", l + 4*k + 4*4*j + 4*4*4*i);

printf("\n");
*/
