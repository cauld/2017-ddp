#include "matrix.h"

#include <stdlib.h>
#include <assert.h>


Matrix *initMatrix(size_t height, size_t width)
{
    Matrix *array = malloc(sizeof(Matrix));

    if(!array)
    {
        perror("malloc");
        return NULL;
    }

    array->height = height;
    array->width = width;

    array->data = calloc(height * width, sizeof(wordID));

    if(!(array->data))
    {
        perror("calloc");
        free(array);
        return NULL;
    }

    return array;
}

void freeMatrix(Matrix *mat)
{
    if(mat)
    {
        free(mat->data);
        free(mat);
    }
}

void setValue(Matrix *mat, wordID value, size_t row, size_t column)
{
    assert(mat && "fonction setValue : pointeur NULL");
    assert((row < mat->height) && (column < mat->width) && "fonction setValue : index hors limites");

    mat->data[row * mat->width + column] = value;
}

wordID getValue(Matrix *mat, size_t row, size_t column)
{
    assert(mat && "fonction setValue : pointeur NULL");
    assert((row < mat->height) && (column < mat->width) && "fonction setValue : index hors limites");

    return mat->data[row * mat->width + column];
}

void displayMatrix(Matrix *mat)
{
    for(size_t row = 0; row < mat->height; row++)
        for(size_t col = 0; col < mat->width; col++)
        {
            printf("%2d ", mat->data[row * mat->width + col]);

            if(col == mat->width - 1)
                printf("\n");
        }
}
