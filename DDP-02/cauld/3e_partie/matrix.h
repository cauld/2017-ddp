#ifndef MATRIX_H
#define MATRIX_H

#include <stdio.h>
#include "defi.h"


typedef struct
{
    wordID *data;
    size_t height;
    size_t width;
} Matrix;

// initialise les coefficients un tableau 2D à zero
Matrix *initMatrix(size_t height, size_t width);
void freeMatrix(Matrix *mat);
void setValue(Matrix *mat, wordID value, size_t row, size_t column);
wordID getValue(Matrix *mat, size_t row, size_t column);
void displayMatrix(Matrix *mat);

#endif // MATRIX_H
