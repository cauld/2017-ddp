#ifndef PATH_H
#define PATH_H

#include <stdbool.h>

#include "matrix.h"
#include "defi.h"

typedef struct
{
    Matrix *prev;
    size_t *prevLength;
} Path;

Path *dijkstra(Graph *gr, wordID source);
void freePath(Path *path);
uint distmin(uint *dist, bool *Q, size_t size);
int displayShortestPath(Graph *gr, Path *path, wordID target);


#endif // PATH_H
