#include "path.h"

#include <limits.h>
#include <stdlib.h>


enum
{
    UNDEFINED = 0,
    EMPTY = 0
};


Path *dijkstra(Graph *gr, wordID source)
{
    // algorithme : https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm#Pseudocode
    // modifié pour trouver tous les chemins les plus courts

    size_t Qsize = gr->headSize;
    Path *path = NULL;
    Matrix *prev = NULL;
    uint *dist = NULL;
    bool *Q = NULL;
    size_t *prevLength = NULL;

    path = malloc(sizeof(Path));
    if(!path)
        goto dijkstraError;

    prev = initMatrix(gr->headSize, gr->headSize);
    if(!prev)
        goto dijkstraError;

    dist = malloc((gr->headSize) * sizeof(uint));
    if(!dist)
        goto dijkstraError;

    Q = malloc((gr->headSize) * sizeof(bool));
    if(!Q)
        goto dijkstraError;

    prevLength = malloc((gr->headSize) * sizeof(size_t));
    if(!prevLength)
        goto dijkstraError;



    // initialisation
    for(size_t i = 0; i < gr->headSize; i++)
    {
        dist[i] = UINT_MAX;

        for(size_t j = 0; j < gr->headSize; j++)
            setValue(prev, UNDEFINED, i, j);

        prevLength[i] = EMPTY;
        Q[i] = true;
    }


    // noeud de depart
    dist[source] = 0;

    while(Qsize > 0)
    {
        uint u = distmin(dist, Q, gr->headSize);

        Q[u] = false;
        Qsize--;

        for(size_t i = 0; i < gr->head[u]->adjNodeArraySize; i++)
        {
            uint v = gr->head[u]->adjNodeArray[i];

            if(Q[v])
            {
                uint alt = dist[u] + 1; // pour le défi, les bonds (distances) sont de 1

                if(alt < dist[v])
                {
                    dist[v] = alt;
                    setValue(prev, u, 0, v);
                    prevLength[v] = 1;
                }
                else if(alt == dist[v])
                {
                    setValue(prev, u, prevLength[v], v);
                    prevLength[v]++;
                }
            }
        }
    }

    free(Q);
    free(dist);

    path->prev = prev;
    path->prevLength = prevLength;

    return path;


dijkstraError:
    perror("malloc");
    free(prevLength);
    free(Q);
    free(dist);
    freeMatrix(prev);
    free(path);
    return NULL;
}

void freePath(Path *path)
{
   if(path)
   {
       free(path->prevLength);
       freeMatrix(path->prev);
       free(path);
   }
}

uint distmin(uint *dist, bool *Q, size_t size)
{
    uint mini = INT_MAX;
    uint index = 0;

    for(size_t i = 0; i < size; i++)
    {
        if((dist[i] < mini) && Q[i])
        {
            mini = dist[i];
            index = (uint)i;
        }
    }

    return index;
}

int displayShortestPath(Graph *gr, Path *path, wordID target)
{
    // algorithme : https://en.wikipedia.org/wiki/Depth-first_search#Pseudocode
    // algorithme non-récursif
    // modifié pour afficher tous les chemins les plus courts trouver par l'algorithme Dijkstra

    size_t sp = 0;

    // dans le pire des cas, le chemin le plus long est de longueur le nombre de noeuds
    // dans la pile, on stocke aussi la profondeur des noeuds donc la taille de la pile
    // est la suivante
    wordID *stack = malloc(2 * (gr->headSize) * sizeof(wordID));
    if(!stack)
    {
        perror("malloc");
        return -1;
    }

    wordID *displayArray = malloc((gr->headSize) * sizeof(wordID));
    if(!displayArray)
    {
        perror("malloc");
        free(stack);
        return -1;
    }


    stack[sp++] = target; // push value
    stack[sp++] = 0; // push depth

    while(sp)
    {
        uint depth = stack[--sp]; // pop depth / wordID alias uint défini dans defi.h
        wordID v = stack[--sp]; // pop value

        displayArray[depth] = v;

        for(size_t i = 0; i < path->prevLength[v]; i++)
        {
            stack[sp++] = getValue(path->prev, i, (size_t)v);
            stack[sp++] = depth + 1;
        }

        if(!path->prevLength[v])
        {
            for(size_t i = 0; i < depth; i++)
                //printf("%2d -- ", array[i]);
                printf("%s--", getWord(gr->dico_ref, displayArray[i]));


            //printf("%2d", array[depth]);
            printf("%s", getWord(gr->dico_ref, displayArray[depth]));
            printf("\n\n");
        }
    }

    free(displayArray);
    free(stack);

    return 0;
}
