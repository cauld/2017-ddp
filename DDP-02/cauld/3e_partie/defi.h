#ifndef DEFI_H
#define DEFI_H

#define WORD_SIZE 5


#include <stdio.h>

typedef unsigned int uint;
typedef unsigned int wordID;

typedef struct
{
    char *data;
    size_t dictionarySize;
} Dictionary;

typedef struct
{
    //wordID id;
    wordID *adjNodeArray;
    size_t adjNodeArraySize;
} Node;

typedef struct
{
    Node **head;
    size_t headSize; // nodeSize = dictionarySize, il y a autant de noeuds que de mots dans le dictionnaire
                     // cette variable est quand meme creee pour une meilleur comprehension
    Dictionary *dico_ref; // embarque une référence sur le dictionnaire
} Graph;

Dictionary *loadTextFile(const char* filename);
void freeDictionary(Dictionary *dico);
char *getWord(const Dictionary *dico, wordID id);
uint distance(const char *s1, const char *s2);
Node *seekNode(Dictionary *dico, wordID id);
void freeNode(Node *no);
Graph *rabbitJump(Dictionary *dico);
void freeGraph(Graph *gr);
int wordToId(Graph *gr, const char *word);
void display(Graph *gr, const char *word);

#endif // DEFI_H
