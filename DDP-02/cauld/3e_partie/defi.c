#include "defi.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

Dictionary *loadTextFile(const char* filename)
{
    Dictionary *data = malloc(sizeof(Dictionary));

    if(!data)
    {
        perror("malloc");
        return NULL;
    }


    FILE *fp = fopen(filename, "r");

    if(!fp)
    {
        perror("fopen");
        free(data);
        return NULL;
    }

    fseek(fp,0, SEEK_END);
    size_t length = (size_t)ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // pas de length+1 car après le dernier mot dans le fichier, il y a un retour à la ligne
    // bon on l'ajoute quand meme
    char *buffer = calloc((length + 1), sizeof(char));

    if(!buffer)
    {
        perror("calloc");
        fclose(fp);
        free(data);
        return NULL;
    }


    int c;
    size_t i = 0;

    while((c = getc(fp)) != EOF)
    {
        if(c != '\n')
            buffer[i] = (char)c;

        else
            buffer[i] = '\0';

        i++;
    }


    fclose(fp);

    data->data = buffer;
    data->dictionarySize = length / (WORD_SIZE + 1);

    return data;
}

void freeDictionary(Dictionary *dico)
{
    if(dico)
    {
        free(dico->data);
        free(dico);
    }
}

// pre-condition id < taille du dictionnaire
char *getWord(const Dictionary *dico, wordID id)
{
    assert(id < (wordID)dico->dictionarySize && "fonction getWord : index hors limite");

    return dico->data + (WORD_SIZE + 1) * id;
}

// pre-condition s1 et s2 doivent avoir la meme longueur
uint distance(const char *s1, const char *s2)
{
    assert(strlen(s1) == strlen(s2) && "fonction distance: les deux chaines doivent avoir la meme longeueur");

    uint sum = 0;

    for(size_t i = 0; i < strlen(s1); i++)
        if(s1[i] != s2[i])
            sum++;

    return sum;
}

// pre-condition id < taille du dictionnaire
Node *seekNode(Dictionary *dico, wordID id)
{
    assert(id < (wordID)dico->dictionarySize && "fonction seekNode : index hors limite");

    Node *no = malloc(sizeof(Node));

    if(!no)
    {
        perror("malloc");
        return NULL;
    }

    size_t count = 0;

    // détermine la longueur de no->adjNodeArraySize, soit le nombre de mot qui sont à une distance de 1 (bond)
    for(size_t i = 0; i < dico->dictionarySize; i++)
    {
        char *temp = getWord(dico, (wordID)i);
        char *temp2 = getWord(dico, (wordID)id);

        if(distance(temp2, temp) == 1)
            count++;
    }

    no->adjNodeArraySize = count;
    no->adjNodeArray = malloc((no->adjNodeArraySize) * sizeof(wordID));

    if(!(no->adjNodeArray))
    {
        perror("malloc");
        free(no);
        return NULL;
    }


    size_t index = 0;

    for(size_t i = 0; i < dico->dictionarySize; i++)
    {
        char *temp = getWord(dico, (wordID)i);
        char *temp2 = getWord(dico, (wordID)id);

        if(distance(temp2, temp) == 1)
        {
            no->adjNodeArray[index] = (wordID)i;
            index++;
        }
    }

    return no;
}

void freeNode(Node *no)
{
    if(no)
    {
        free(no->adjNodeArray);
        free(no);
    }
}

Graph *rabbitJump(Dictionary *dico)
{
    Graph *gr = malloc(sizeof(Graph));

    if(!gr)
    {
        perror("malloc");
        return NULL;
    }


    gr->dico_ref = dico;
    gr->headSize = gr->dico_ref->dictionarySize;
    gr->head = malloc((gr->headSize) * sizeof(Node*));

    if(!(gr->head))
    {
        perror("malloc");
        free(gr);
        return NULL;
    }

    for(size_t i = 0; i < gr->headSize; i++)
        gr->head[i] = NULL;


    for(size_t i = 0; i < gr->headSize; i++)
    {
        gr->head[i] = seekNode(gr->dico_ref, (wordID)i);

        // ménage en cas d'erreur
        if(!(gr->head[i]))
        {
            perror("malloc");

            for(size_t j = 0; j < gr->headSize; j++)
                freeNode(gr->head[j]);

            free(gr->head);
            free(gr);
            return NULL;
        }
    }

    return gr;
}

void freeGraph(Graph *gr)
{
    if(gr)
    {
        for(size_t i = 0; i < gr->headSize; i++)
            freeNode(gr->head[i]);

        free(gr->head);
        free(gr);
    }
}

int wordToId(Graph *gr, const char *word)
{
    int index = -1;

    for(size_t i = 0; i< gr->headSize; i++)
    {
        char *temp = getWord(gr->dico_ref, (wordID)i);
        if(!strcmp(word, temp))
        {
            index = (int)i;
            break;
        }
    }

    return index;
}

void display(Graph *gr, const char *word)
{
    int id = wordToId(gr, word);

    printf("%s : ", word);

    if(id != -1)
        for(size_t i = 0; i < gr->head[id]->adjNodeArraySize; i++)
            printf("%s ", getWord(gr->dico_ref, gr->head[id]->adjNodeArray[i]));


    printf("\n\n");
}
