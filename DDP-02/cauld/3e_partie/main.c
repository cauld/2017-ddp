#include "defi.h"
#include "path.h"

#include <string.h>

int main(void)
{
    Path *path = NULL;

    Dictionary *data = loadTextFile("FR_len05.dico");
    if(!data)
		return -1;


    Graph *gr = rabbitJump(data);
    if(!gr)
    {
        freeDictionary(data);
        return -1;
    }


    printf("EXIT pour quitter\n");

    bool quit = false;

    while(!quit)
    {
        char word1[WORD_SIZE + 1];
        char word2[WORD_SIZE + 1];

        printf("1er mot : ");
        scanf("%5s", word1);

        int c;
        while((c = getchar()) != EOF && c != '\n');

        if(strcmp(word1, "EXIT"))
        {
            printf("2e mot : ");
            scanf("%5s", word2);

            while((c = getchar()) != EOF && c != '\n');


            if(!strcmp(word2, "EXIT"))
                quit = true;

            else if((strlen(word1) != WORD_SIZE) || (strlen(word1) != WORD_SIZE))
                printf("le ou les mots doivent avoir %d lettres\n\n", WORD_SIZE);

            else
            {
                int wsource = wordToId(gr, word1);
                int wtarget = wordToId(gr, word2);

                if((wsource == -1) || (wtarget == -1))
                    printf("l'un des deux ou les deux mots ne sont pas dans le dictionnaire\n\n");

                else if(!strcmp(word1, word2))
                    printf("aucun chemin car les deux mots sont identiques\n\n");

                else
                {
                    path = dijkstra(gr, (wordID)wtarget);
                    if(!path)
                    {
                        freeGraph(gr);
                        freeDictionary(data);
                        return -1;
                    }

                    int error = displayShortestPath(gr, path, (wordID)wsource);
                    if(error)
                    {
                        freePath(path);
                        freeGraph(gr);
                        freeDictionary(data);
                        return -1;
                    }

                    freePath(path);
                    path = NULL;
                }
            }
        }
        else
            quit = true;
    }

    freePath(path);
    freeGraph(gr);
    freeDictionary(data);

    return 0;

}
